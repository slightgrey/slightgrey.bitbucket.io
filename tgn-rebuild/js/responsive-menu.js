jQuery(function($){
    
    $('.menu-mobile').click(function() {
        $('.mobile-menu .container .mobile-header').hide();
        $('.mobile-menu-wrap').show();
    });
    
    $('.close-mobile').click(function() {
        $('.mobile-menu .container .mobile-header').show();
        $('.mobile-menu-wrap').hide();
    });
});